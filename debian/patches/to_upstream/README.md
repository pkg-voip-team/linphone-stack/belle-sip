The patches contained in this directory are considered suitable for merging upstream.

You can follow changes to this directory through [its Atom web feed](https://salsa.debian.org/pkg-voip-team/linphone-stack/belle-sip/-/commits/master/debian/patches/to_upstream?format=atom) (recommended polling interval: 24 hours).
